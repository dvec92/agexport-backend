﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Poc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poc.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class EncabezadoController : ControllerBase
    {

        private readonly PocContext _context;
        
        public EncabezadoController(PocContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Encabezado> getAll()
        {
            return _context.Encabezado.Include("detalles").ToList();
        }

        [HttpGet("{id}")]
        public Encabezado getEncabezadoById(int id)
        {
            return _context.Encabezado.Find(id);
        }

        [HttpPost]
        public Encabezado saveEncabezado(Encabezado encabezado)
        {
            EntityEntry<Encabezado> encabezadoCreated;
            encabezadoCreated = _context.Encabezado.Add(encabezado);
            _context.SaveChanges();
            return encabezadoCreated.Entity;
        }

        [HttpPut("{id}")]
        public Encabezado updateEncabezado(int id, Encabezado encabezado)
        {
            Encabezado encabezadoUpd;
            encabezadoUpd = _context.Encabezado.First(enc => enc.ideFactura == id);
            encabezadoUpd.nombre = encabezado.nombre;
            encabezadoUpd.nit = encabezado.nit;
            encabezadoUpd.fecha = encabezado.fecha;
            encabezadoUpd.correlativo = encabezado.correlativo;
            foreach (Detalle det in _context.Detalle.Where<Detalle>(det => det.ideFactura == id))
            {
                _context.Detalle.Remove(det);
            }
            encabezadoUpd.detalles = encabezado.detalles;
            
            _context.SaveChanges();
            return encabezadoUpd;
        }

        [HttpDelete("{id}")]
        public void deleteEncabezado(int id)
        {
            Encabezado encabezado = _context.Encabezado.Find(id);
            foreach(Detalle det in _context.Detalle.Where<Detalle>(det => det.ideFactura == id))
            {
                _context.Detalle.Remove(det);
            }
            _context.Encabezado.Remove(encabezado);
            _context.SaveChanges();
        }

    }
}
