﻿using Microsoft.AspNetCore.Mvc;
using Poc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poc.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleController : ControllerBase
    {

        private readonly PocContext _context;

        public DetalleController(PocContext context)
        {
            _context = context;
        }


        [HttpGet("encabezado/{id}")]
        public IQueryable<Detalle> getDetallesByIdEncabezado(int id)
        {
            return _context.Detalle.Where(det => det.encabezado.ideFactura == id);
        }



    }
}
