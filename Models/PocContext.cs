﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Poc.Models
{
    public class PocContext : DbContext
    {

        public PocContext(DbContextOptions<PocContext> options): base (options)
        {

        }

        public DbSet<Encabezado> Encabezado { get; set; }
        public DbSet<Detalle> Detalle { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Detalle>().HasOne(enc => enc.encabezado).WithMany(det => det.detalles).HasForeignKey(enc => enc.ideFactura);
        }
    }
}
