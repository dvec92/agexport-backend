﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Poc.Models
{
    [Table("fac_encabezado")]
    public class Encabezado
    {

        [Key]
        [Column(name:"ide_factura")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ideFactura { get; set; }

        [Column(name:"nombre_cliente")]
        public string nombre { get; set; }
        
        [Column(name:"nit")]
        public string nit { get; set; }

        [Column(name: "fecha")]
        public DateTime fecha { get; set; }

        [Column(name: "correlativo")]
        public string correlativo { get; set; }

        public List<Detalle> detalles { get; set; }

    }
}
