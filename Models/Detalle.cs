﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Poc.Models
{
    [Table("fac_detalle")]
    public class Detalle
    {
        [Key]
        [Column("ide_detalle")]
        public int ideDetalle { get; set; }

        [Column("ide_factura")]
        public int ideFactura { get; set; }

        [Column("producto")]
        public string producto { get; set; }

        [Column("cantidad")]
        public int cantidad { get; set; }

        [Column("monto")]
        public decimal monto { get; set; }

        [JsonIgnore]
        public Encabezado encabezado { get; set; }

    }
}
